<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    use HasFactory;

    public function getActivityAttribute()
    {

        return $this->active ? 'Active  ' : 'In-Acive';
    }
}
