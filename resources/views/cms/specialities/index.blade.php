@extends('cms.parent')

@section('title','Specialites')
@section('page-title','Specialites')
@section('small-page-title','Index')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Responsive Hover Table</h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control float-right"
                                    placeholder="Search">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover table-bordered text-nowrap">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title en</th>
                                    <th>Title Ar</th>
                                    <th>Status</th>
                                    <th>Updated at</th>
                                    <th>Created at</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            {{--  --}}
                            <tbody>
                                @foreach ( $specialities as $speciality)
                                <tr>
                                    <td>{{$speciality->id}}</td>
                                    <td>{{$speciality->title_en}}</td>
                                    <td>{{$speciality->title_ar}}</td>

                                    @if($speciality->active)

                                    <td> <span class="badge bg-success">{{$speciality->activity}}</span></td>
                                    @else
                                    <td> <span class="badge bg-danger">{{$speciality->activity}}</span></td>
                                    @endif

                                    <td>{{$speciality->created_at}}</td>
                                    <td>{{$speciality->updated_at}}</td>
                                    <td><button type="button" class="btn btn-danger">
                                            <i class="fas fa-trash-alt"> </i>
                                        </button>
                                        <button type="button" class="btn btn-info">
                                            <i class="fas fa-edit"> </i>

                                        </button>
                                    </td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>

@endsection