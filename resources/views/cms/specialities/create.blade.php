@extends('cms.parent')

@section('title','Specialities')
@section('page-title','Speciality')
@section('small-page-title','create speciality')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Speciality</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="POST" action="{{route('specialities.store')}}">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="title_en">Title English</label>
                                <input type="text" name="title_en" class="form-control" id="title_en"
                                    placeholder="Enter Englis Title">
                            </div>
                            <div class="form-group">
                                <label for="title_ar">Title Arbic</label>
                                <input type="text" name="title_ar" class="form-control" id="title_ar"
                                    placeholder="Enter Arabic Title">
                            </div>

                            <div class="form-check">
                                <input type="checkbox" name="active" class="form-check-input" id="active">
                                <label class="form-check-label" for="active">Active</label>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Store</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->






            </div>
            <!--/.col (left) -->

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->


@endsection